var UDISObtenidos;

function getUDIS(serie) {
  var url = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/" + serie + "/datos/oportuno?token=99c0dab06956cd30809fca4d0365ba9f1262433ee6e278344026a0e560b43e8d";

  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      UDISObtenidos = request.responseText;
      tablaUsuarios.style.display = 'none';
      navbarSupportedContent.load;
      tablaCuentas.style.display = 'none';
      tablaMovimientos.style.display = 'none';
      prueba.style.display = "block"
      alta.style.display = "none"        
    //  location.reload(true);
      procesarUDIS();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarUDIS(){
  var JSONUDIS = JSON.parse(UDISObtenidos);
  var JSONUDIS2 = JSONUDIS.bmx.series;
  var divTabla = document.getElementById("prueba");
  var tabla = document.createElement("table");
  var tbody =document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i = 0; i < JSONUDIS2.length+1; i++) {
    var nuevaFila = document.createElement("tr");
    if (i==0) {
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = "IdSerie";
      var columnatitulo = document.createElement("td");
      columnatitulo.innerText = "Titulo";
      var columnaFecha = document.createElement("td");
      columnaFecha.innerText = "Fecha";
      var columnaValor = document.createElement("td");
      columnaValor.innerText = "Valor";
    } else {
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONUDIS2[i-1].idSerie;
      var columnatitulo = document.createElement("td");
      columnatitulo.innerText = JSONUDIS2[i-1].titulo;
      var columnaFecha = document.createElement("td");
      columnaFecha.innerText = JSONUDIS2[i-1].datos[0].fecha;
      var columnaValor = document.createElement("td");
      columnaValor.innerText = JSONUDIS2[i-1].datos[0].dato;
    }
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnatitulo);
    nuevaFila.appendChild(columnaFecha);
    nuevaFila.appendChild(columnaValor);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
