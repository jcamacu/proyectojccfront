var usuariosObtenidos;

function getUsuarios() {
  var url = "http://localhost:3000/api/v1/urecupera";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      usuariosObtenidos = request.responseText;
      tablaUsuarios.style.display = 'block';
      tablaCuentas.style.display = 'none';
      navbarSupportedContent.load;
      tablaMovimientos.style.display = 'none';
      prueba.style.display = "none"
      alta.style.display = "none"
      procesarUsuarios();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarUsuarios(){
  var JSONUsuarios = JSON.parse(usuariosObtenidos);
  var divTabla = document.getElementById("tablaUsuarios");
  var tabla = document.createElement("table");
  var tbody =document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONUsuarios.length+1; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaID = document.createElement("td");

    if (i==0) {
      var columnaID = document.createElement("td");
      columnaID.innerText = "ID";
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = "Nombre";
      var columnaApellido = document.createElement("td");
      columnaApellido.innerText = "Apellido";
      var columnaemail = document.createElement("td");
      columnaemail.innerText = "Email";
      var columnaPassword = document.createElement("td");
      columnaPassword.innerText = "Password";
    } else {
    var columnaID = document.createElement("td");
    columnaID.innerText = JSONUsuarios[i-1].idcon;
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONUsuarios[i-1].nombre;
    var columnaApellido = document.createElement("td");
    columnaApellido.innerText = JSONUsuarios[i-1].apellido;
    var columnaemail = document.createElement("td");
    columnaemail.innerText = JSONUsuarios[i-1].email;
    var columnaPassword = document.createElement("td");
    columnaPassword.innerText = JSONUsuarios[i-1].password;
    }
    nuevaFila.appendChild(columnaID);
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaApellido);
    nuevaFila.appendChild(columnaemail);
    nuevaFila.appendChild(columnaPassword);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
