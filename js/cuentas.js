var cuentasObtenidos;

function getCuentas() {
  var url = "http://localhost:3000/api/v1/crecupera";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      cuentasObtenidos = request.responseText;
      tablaUsuarios.style.display = 'none';
      navbarSupportedContent.load;
      tablaCuentas.style.display = 'block';
      tablaMovimientos.style.display = 'none';
      prueba.style.display = "none"
      alta.style.display = "none"
      //location.reload(true);
      procesarCuentas();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarCuentas(){
  var JSONCuentas = JSON.parse(cuentasObtenidos);
  var divTabla = document.getElementById("tablaCuentas");
  var tabla = document.createElement("table");
  var tbody =document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONCuentas.length+1; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaID = document.createElement("td");
    if (i==0) {
      var columnaCliente = document.createElement("td");
      columnaCliente.innerText = "Cliente";
      var columnaCuenta = document.createElement("td");
      columnaCuenta.innerText = "Cuenta";
      var columnaTipo = document.createElement("td");
      columnaTipo.innerText = "Tipo";
      var columnaDescripcion = document.createElement("td");
      columnaDescripcion.innerText = "Descripcion";
      var columnaUsuario = document.createElement("td");
      columnaUsuario.innerText = "Usuario";
      var columnaFecha = document.createElement("td");
      columnaFecha.innerText = "Fecha";
    } else {
    var columnaCliente = document.createElement("td");
    columnaCliente.innerText = JSONCuentas[i-1].cliente;
    var columnaCuenta = document.createElement("td");
    columnaCuenta.innerText = JSONCuentas[i-1].cuenta;
    var columnaTipo = document.createElement("td");
    columnaTipo.innerText = JSONCuentas[i-1].tipo;
    var columnaDescripcion = document.createElement("td");
    columnaDescripcion.innerText = JSONCuentas[i-1].descripcion;
    var columnaUsuario = document.createElement("td");
    columnaUsuario.innerText = JSONCuentas[i-1].usuario;
    var columnaFecha = document.createElement("td");
    columnaFecha.innerText = JSONCuentas[i-1].fecharegistro;
    }
    nuevaFila.appendChild(columnaCliente);
    nuevaFila.appendChild(columnaCuenta);
    nuevaFila.appendChild(columnaTipo);
    nuevaFila.appendChild(columnaDescripcion);
    nuevaFila.appendChild(columnaUsuario);
    nuevaFila.appendChild(columnaFecha);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
