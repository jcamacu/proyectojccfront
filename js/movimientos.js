var movimientosObtenidos;

function getMovimientos() {
  var url = "http://localhost:3000/api/v1/mrecupera";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      movimientosObtenidos = request.responseText;
      tablaUsuarios.style.display = 'none';
      navbarSupportedContent.load;
      tablaCuentas.style.display = 'none';
      tablaMovimientos.style.display = 'block';
      prueba.style.display = "none"
      alta.style.display = "none"
      procesarMovimientos();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarMovimientos(){
  var JSONMovimientos = JSON.parse(movimientosObtenidos);
  var divTabla = document.getElementById("tablaMovimientos");
  var tabla = document.createElement("table");
  var tbody =document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONMovimientos.length+1; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaID = document.createElement("td");
    if (i==0) {
      var columnaCliente = document.createElement("td");
      columnaCliente.innerText = "Cliente";
      var columnaCuenta = document.createElement("td");
      columnaCuenta.innerText = "Cuenta";
      var columnaID = document.createElement("td");
      columnaID.innerText = "ID";
      var columnaFecha = document.createElement("td");
      columnaFecha.innerText = "Fecha";
      var columnaImporte = document.createElement("td");
      columnaImporte.innerText = "Importe";
      var columnaTipo = document.createElement("td");
      columnaTipo.innerText = "Tipo";
      var columnaUsuario = document.createElement("td");
      columnaUsuario.innerText = "Usuario";
      var columnaFecharegistro = document.createElement("td");
      columnaFecharegistro.innerText = "Fecha";
    } else {
      var columnaCliente = document.createElement("td");
      columnaCliente.innerText = JSONMovimientos[i-1].cliente;
      var columnaCuenta = document.createElement("td");
      columnaCuenta.innerText = JSONMovimientos[i-1].cuenta;
      var columnaID = document.createElement("td");
      columnaID.innerText = JSONMovimientos[i-1].idmov;
      var columnaFecha = document.createElement("td");
      columnaFecha.innerText = JSONMovimientos[i-1].fecha;
      var columnaImporte = document.createElement("td");
      columnaImporte.innerText = JSONMovimientos[i-1].importe;
      var columnaTipo = document.createElement("td");
      columnaTipo.innerText =  JSONMovimientos[i-1].tipo;
      var columnaUsuario = document.createElement("td");
      columnaUsuario.innerText =  JSONMovimientos[i-1].usuario;
      var columnaFecharegistro = document.createElement("td");
      columnaFecharegistro.innerText =  JSONMovimientos[i-1].fecharegistro;
    }
    nuevaFila.appendChild(columnaCliente);
    nuevaFila.appendChild(columnaCuenta);
    nuevaFila.appendChild(columnaID);
    nuevaFila.appendChild(columnaFecha);
    nuevaFila.appendChild(columnaImporte);
    nuevaFila.appendChild(columnaTipo);
    nuevaFila.appendChild(columnaUsuario);
    nuevaFila.appendChild(columnaFecharegistro);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
