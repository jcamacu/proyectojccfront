//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 2000;

var path = require('path');
app.use(express.static(__dirname + '/build/default'))

var bodyparser = require('body-parser')
app.use(bodyparser.json())

app.listen(port);

console.log('Ejecutando mi proyecto Practitioner: ' + port);

app.get('/',function (req, res) {
  //res.send("Hola Mundo nodeJS")
  //res.sendFile(path.join(__dirname,'index.html'));
  res.sendFile("index.html",{root: '.'});
});
